# PROJET_Andia_Colomines_Haltout

**Ecriture d'un mini SGBDR en java dans le cadre des TP de BDD Avancée**

L3 Paris descartes 2018
Andia Colomines Haltout

Projet eclipse : Code
Dossier cible pour la creation du .jar par eclipse : executable

**Dossier contenant l'archive rendu sur moodle : archive finale**

Scripts à lancer dans un terminal (.sh pour linux et mac, .bat pour windows), java n'est pas livré avec
repertoire de la BDD : DB,
repertoire contenant le .jar executable : JAR
repertoire contenant les sources : Code

Pour le lancement de l'executable sans passer par les scripts :
prends un argument : l'emplacement du dossier contenant les fichiers de la bdd (le dossier doit exister),
sans arguments les fichiers de la bdd sont stockés à côté du .jar



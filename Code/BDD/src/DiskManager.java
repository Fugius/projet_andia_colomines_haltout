import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.io.File;
import java.io.RandomAccessFile;
import java.io.IOException;

public class DiskManager {

	private static final DiskManager instance = new DiskManager();
	
	 public static DiskManager getMonSingleton() {
	        return instance;
	    }
	 
	 public void CreateFile(int iFileIdx)
	 {
		 File file = new File(Main.cheminDB +iFileIdx + ".rf");
		 try{
			 file.createNewFile();
		 }
		 catch(IOException e)
		 {
			 System.out.println(e.getMessage());
		 }
		 
	 }
	 
	 public void AddPage(int iFileIdx, PageId oPageId)
	 {		 
		 //Lire le bon fichier, en deduire le nombre de pages pour connaitre oPageId puis ajouter les 0 et modifier oPageId
 
		 try {
			 RandomAccessFile file = new RandomAccessFile(Main.cheminDB + iFileIdx + ".rf", "rw");
			 file.seek(file.length());
			 for (int i = 0; i < Constants.pageSize; i++) {
				 file.write(0b0);
			 }
			 
			 oPageId.setFileIdx(iFileIdx);
			 oPageId.setPageIdx(((int)file.length()/Constants.pageSize) - 1);
			 file.close();
			 
		 } catch (Exception e) {
			 System.out.println(e.getMessage());
		 }
		 		 
	 }
	 
	 public void ReadPage(PageId iPageId, ByteBuffer oBuffer)
	 {
		 		 
		 try {
			RandomAccessFile file = new RandomAccessFile(Main.cheminDB + iPageId.getFileIdx() + ".rf", "rw");
			file.seek((iPageId.getPageIdx() + 1) * Constants.pageSize);			
			file.read(oBuffer.array());
			file.close();
			 
		 } catch (Exception e) {
			System.out.println(e.getMessage());
		 }
		 
	 }
	 
	 public void WritePage(PageId iPageId, ByteBuffer iBuffer)
	 {
		 
		 try {
			 RandomAccessFile file = new RandomAccessFile(Main.cheminDB + iPageId.getFileIdx() + ".rf", "rw");
			 file.seek((iPageId.getPageIdx() + 1) * Constants.pageSize);
			 file.write(iBuffer.array());
			 file.close();
			 
		 } catch (Exception e) {
			 System.out.println(e.getMessage());
		 }
	 }
}



public class DataPageInfo {
	private int PageIdx;	
	private int freeSlots;
	
	public int getPageIdx() {
		return PageIdx;
	}
	public void setPageIdx(int pageIdx) {
		PageIdx = pageIdx;
	}
	public int getFreeSlots() {
		return freeSlots;
	}
	public void setFreeSlots(int freeSlots) {
		this.freeSlots = freeSlots;
	}
}

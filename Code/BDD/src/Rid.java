
public class Rid {

	private PageId pageId;
	private int smotIdx;
	
	
	public PageId getPageId() {
		return pageId;
	}
	
	public void setPageId(PageId pageId) {
		this.pageId = pageId;
	}
	
	public int getSmotIdx() {
		return smotIdx;
	}
	
	public void setSmotIdx(int smotIdx) {
		this.smotIdx = smotIdx;
	}
}

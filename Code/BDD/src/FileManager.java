import java.util.ArrayList;

public final class FileManager {
	
	private static final FileManager instance = new FileManager(); 
	private ArrayList<HeapFile> hfiles = new ArrayList<>();
	
	public FileManager(){	
		//..
	}
	
	public void reset() {
		hfiles = new ArrayList<>();
	}
	
	public static FileManager getMonSingleton() {
		return instance;
	}
	
	public void init() {
		ArrayList<RelDef> a = DBDef.getMonSingleton().getRelDefs();
		
		for(int i = 0; i < a.size(); i++) {
			HeapFile hf = new HeapFile();
			hf.setRelDef(a.get(i));
			hfiles.add(hf);
		}
	}

	public void createNewHeapFile(RelDef iRelDef) {
		HeapFile hfile = new HeapFile();
		hfile.setRelDef(iRelDef);
		hfile.createNewOnDisk();
		hfiles.add(hfile);
		
	}
	
	public Rid insertRecordInRelation(String iRelationName,Record iRecord ) {
		for(int i = 0; i < hfiles.size(); i++) {
			if (iRelationName.equals(hfiles.get(i).getRelDef().getNomRelation())) {
				return hfiles.get(i).insertRecord(iRecord);
			}
		}
		return null;
	}
	
	public HeapFile getHeapFile(String name ) {
		for (HeapFile hp : hfiles) {
			if (hp.getRelDef().getNomRelation().equals(name))
				return hp;
		}
		
		return null;
	}
	
	public ArrayList<Record> getAllRecords(String str) {
		DBDef db = DBDef.getMonSingleton();
		HeapFile heapFile = null;
		
		ArrayList<Record> results = new ArrayList<>();
				
		for (HeapFile hp : hfiles) {
			if (hp.getRelDef().getNomRelation().equals(str))
				heapFile = hp;
		}
		
		ArrayList<PageId> pid = heapFile.getDataPageIds();
		
		for (PageId p : pid) {
			for (Record r : heapFile.getRecordsOnPage(p)) {
				results.add(r);
			}
		}
		
		return results; 
	}
	
	public ArrayList<Record> getAllRecordsWithFilter(String name, int iIdxCol, String iValeur) {
		ArrayList<Record> recs = getAllRecords(name);
		ArrayList<Record> results = new ArrayList<>();
		
		for (Record rc : recs) {
			if (rc.getValues().get(iIdxCol - 1).equals(iValeur)) {
				results.add(rc);
			}
		}
		
		return results;
	}
}

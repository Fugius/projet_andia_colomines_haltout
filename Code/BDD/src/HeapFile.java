import java.nio.ByteBuffer;
import java.util.ArrayList;

public class HeapFile {
	
	private RelDef relDef;
	private HeaderPageInfo headerPageI = new HeaderPageInfo();
	
	
	
	public HeapFile() {
		
	}
	
	public HeaderPageInfo getHeaderInfos() {
		BufferManager bm = BufferManager.getInstance();

		PageId headerID = new PageId();
		headerID.setFileIdx(relDef.getFileIdx());
		headerID.setPageIdx(0);
		
		ByteBuffer headerBuffer = bm.getPage(headerID);
		headerPageI.readFromBuffer(headerBuffer);
		
		bm.freePage(headerID, true);

		return headerPageI;
	}
	
	public void createNewOnDisk() {
		DiskManager dm = DiskManager.getMonSingleton();
		BufferManager bm = BufferManager.getInstance();
		
		HeaderPageInfo headerPageI = new HeaderPageInfo();		
		
		PageId headerPageID = new PageId();
		headerPageID.setFileIdx(relDef.getFileIdx());
		
		dm.CreateFile(relDef.getFileIdx());
		dm.AddPage(relDef.getFileIdx(), headerPageID);
		
		ByteBuffer bb = bm.getPage(headerPageID);
		
		headerPageI.setDataPageCount(0);
		headerPageI.writeToBuffer(bb);
		
		bm.freePage(headerPageID, true);
	}
	
	public void getFreePageId(PageId oPageId) {
		BufferManager bm = BufferManager.getInstance();
		
		PageId headerID = new PageId();
		headerID.setFileIdx(relDef.getFileIdx());
		headerID.setPageIdx(0);
		
		ByteBuffer headerBuffer = bm.getPage(headerID);
		headerPageI.readFromBuffer(headerBuffer);
		
		int freePageId = -1;
		
		for (DataPageInfo dpi : headerPageI.getListDataPageInfo()) {
			if (dpi.getFreeSlots() > 0) {
				freePageId = dpi.getPageIdx();
				break;
			}
		}
		
		if (freePageId > -1) {
			oPageId.setFileIdx(headerID.getFileIdx());
			oPageId.setPageIdx(freePageId);
			
			bm.freePage(headerID, false);
			
		} else {
			DiskManager dm = DiskManager.getMonSingleton();
			
			dm.AddPage(headerID.getFileIdx(), oPageId);
			ByteBuffer page = bm.getPage(oPageId);
			page.position(0);
			
			for (int i = 0; i < relDef.getSlotCount(); i++) {
				page.put((byte)0b0);
			}
			
			DataPageInfo dpi = new DataPageInfo();
			dpi.setFreeSlots(relDef.getSlotCount());
			dpi.setPageIdx(headerPageI.getDataPageCount() + 1);
			
			headerPageI.getListDataPageInfo().add(dpi);
			headerPageI.setDataPageCount(headerPageI.getDataPageCount() + 1);
			headerPageI.writeToBuffer(headerBuffer);
			
			bm.freePage(oPageId, true);
			bm.freePage(headerID, true);
		}
				
	}
	
	public void updateHeaderWithTakenSlot(PageId iPageId) {
		BufferManager bm = BufferManager.getInstance();
		
		PageId headerID = new PageId();
		headerID.setFileIdx(relDef.getFileIdx());
		headerID.setPageIdx(0);
		
		ByteBuffer bb = bm.getPage(headerID);
		headerPageI.readFromBuffer(bb);
		
		for (DataPageInfo dpi : headerPageI.getListDataPageInfo()) {
			if (dpi.getPageIdx() == iPageId.getPageIdx()) {
				dpi.setFreeSlots(dpi.getFreeSlots() - 1);
			}
		}
		
		headerPageI.writeToBuffer(bb);
		bm.freePage(headerID, true);
	}
	
	private void writeRecordInBuffer(Record iRecord, ByteBuffer ioBuffer, int iSlotIdx) {
		ioBuffer.position(iSlotIdx);
		ioBuffer.put((byte)0b1);
		
		int pos = relDef.getSlotCount() + relDef.getRecordSize() * iSlotIdx;
		ioBuffer.position(pos);
		
		for (int i = 0; i < iRecord.getValues().size(); i++) {
			
			String type = relDef.getTypesColonnes().get(i);
			String record = iRecord.getValues().get(i);
			
			if (type.toUpperCase().equals("INT")) {
				int int_record = Integer.valueOf(record);
				ioBuffer.putInt(int_record);
			} else if (type.toUpperCase().equals("FLOAT")) {
				float float_record = Float.valueOf(record);
				ioBuffer.putFloat(float_record);
			} else if (type.toUpperCase().contains("STRING")) {
				for (int j = 0; j < record.length(); j++ ) {
					ioBuffer.putChar(record.charAt(j));
				}
			}
			
		}
		
		
	}
	
	private Rid insertRecordInPage(Record iRecord, PageId iPageId) {
		BufferManager bm = BufferManager.getInstance();
		ByteBuffer buffer = bm.getPage(iPageId);
		buffer.position(0);
		
		Rid rid = new Rid();
		
		byte[] map = new byte[relDef.getSlotCount()];
		buffer.get(map);
		
		int pos = 0;
		
		for (pos = 0; pos < map.length; pos++ ) {
			if (map[pos] == 0b0)
				break;
		}
		
		writeRecordInBuffer(iRecord, buffer, pos);
		
		rid.setPageId(iPageId);
		rid.setSmotIdx(pos);
		
		bm.freePage(iPageId, true);
		updateHeaderWithTakenSlot(iPageId);
		
		return rid;
	}
	
	public Rid insertRecord (Record iRecord) {
		PageId pid = new PageId();
		getFreePageId(pid);
		return insertRecordInPage(iRecord, pid);
	}


	public RelDef getRelDef() {
		return relDef;
	}

	public void setRelDef(RelDef relDef) {
		this.relDef = relDef;
	}
	
	public Record readRecordFromBuffer(ByteBuffer iBuffer, int iSlotIdx) {
		
		Record record = new Record();
		
		int p = relDef.getSlotCount() + relDef.getRecordSize() * iSlotIdx;
		iBuffer.position(p);
		 
		for(String s : relDef.getTypesColonnes() ) {
			if (s.toUpperCase().equals("INT")) {
				int r = iBuffer.getInt();
				record.getValues().add(String.valueOf(r));
			} else if (s.toUpperCase().equals("FLOAT")) {
				float f = iBuffer.getFloat();
				record.getValues().add(String.valueOf(f));
			} else if (s.toUpperCase().contains("STRING")) {
				String size = s.substring(6);
				int a = Integer.valueOf(size);
				String r = ""; 
				for(int i=0;i<a;i++) {
					
					r += iBuffer.getChar();

				}
				record.getValues().add(r);
			}
			
		}
			
		return record;
	}
	
	
	public ArrayList<Record> getRecordsOnPage(PageId iPageId){
		
		BufferManager bm = BufferManager.getInstance();
		
		ByteBuffer b = bm.getPage(iPageId);
		b.position(0);
		
		ArrayList<Record> ListeDeRecords = new ArrayList<Record>();
		
		byte[] map = new byte[relDef.getSlotCount()];
		b.get(map);
		
		for(int i = 0; i<map.length; i++) {
			if(map[i]==0b1) {
				b.position(0);
				ListeDeRecords.add(readRecordFromBuffer(b, i));
            }
			
		}
		
		bm.freePage(iPageId, false);
		return ListeDeRecords;
		
		
	}
	
	public ArrayList<PageId> getDataPageIds() {
		
		PageId headerID = new PageId();
		headerID.setFileIdx(relDef.getFileIdx());
		headerID.setPageIdx(0);
		
		BufferManager bm = BufferManager.getInstance();
		HeaderPageInfo hdi = new HeaderPageInfo();
		
		ArrayList<PageId> ListeDePageId = new ArrayList<PageId>();
		
		hdi.readFromBuffer(bm.getPage(headerID));
		
		for(DataPageInfo dpi : hdi.getListDataPageInfo()) {
			PageId i = new PageId();
			i.setFileIdx(relDef.getFileIdx());
			i.setPageIdx(dpi.getPageIdx());
			ListeDePageId.add(i);
		}
		
		bm.freePage(headerID, false);
		
		return ListeDePageId;
	}

}

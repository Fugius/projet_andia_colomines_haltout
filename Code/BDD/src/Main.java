import java.io.File;
import java.util.Scanner;

public class Main {
	
	public static String cheminDB;
	
	public static void main(String[] args) {
		
		
		if (args != null && args.length > 0 ) {
			cheminDB = args[0];
			
			if (cheminDB.charAt(cheminDB.length() - 1) != '/' && cheminDB.charAt(cheminDB.length() - 1) != '\\') {
				cheminDB += "/";
			}
			
		} else {
			cheminDB = new File(System.getProperty("user.dir")).getAbsolutePath();
		}
		
		DBManager db = new DBManager();
		
		db.init();
		
		String c = "";
		Scanner sc = new Scanner(System.in);
		
		while(true){
			System.out.println("Entrez commande :  ");
			c = sc.nextLine();
			
			if(c.equals("exit")) {
				db.finish();
				sc.close();
				break;
			}
			
			db.processCommand(c);
			System.out.print("\n");

		}	

	}
	
	 
}

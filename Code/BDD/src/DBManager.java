import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;

public final class DBManager {
	
	private static final DBManager instance = new DBManager();
	 
    public DBManager() {
        //...
    }
 
    public static DBManager getMonSingleton() {
        return instance;
    }
	
	
	public void init()
	{
		DBDef.getMonSingleton().Init();
		FileManager.getMonSingleton().init();
	}
	
	public void finish()
	{
		DBDef.getMonSingleton().Finish();
		BufferManager.getInstance().flushAll();
		
	}
	

	public void processCommand(String chaineCommande)
	{
		String [] parts = chaineCommande.split(" ");
		
		if (parts[0].equals("create")) {
			ExecCommandCreate(parts);
		}
		else if(parts[0].equals("fill")) {
			ExecCommandFill(parts);
		} else if (parts[0].equals("insert")) {
			ExecCommandInsert(parts);
		}
		else if(parts [0].equals("selectall")) {
			ExecCommandSelectAll(parts);
		}
		else if(parts [0].equals("select")) {
			ExecCommandSelect(parts);
		} else if (parts[0].equals("clean")) {
			clean();
		} else if (parts[0].equals("join")) {
			join(parts);
		}
	}
	
	private void join(String args[]) {
		String name1 = args[1];
		String name2 = args[2];
		
		int colId1 = Integer.valueOf(args[3]);
		int colId2 = Integer.valueOf(args[4]);
		
		HeapFile hp1 = FileManager.getMonSingleton().getHeapFile(name1); 
		HeapFile hp2 = FileManager.getMonSingleton().getHeapFile(name2); 
		
		HeaderPageInfo head1 = hp1.getHeaderInfos();
		HeaderPageInfo head2 = hp2.getHeaderInfos();
		
		int n = 0;
		
		for (int i = 0; i < head1.getDataPageCount(); i++) {
			PageId pid1 = new PageId();
			pid1.setFileIdx(hp1.getRelDef().getFileIdx());
			pid1.setPageIdx(head1.getListDataPageInfo().get(i).getPageIdx());
			
			ArrayList<Record> recs1 = hp1.getRecordsOnPage(pid1);
			
			for (int j = 0; j < head2.getDataPageCount() ; j++) {
				PageId pid2 = new PageId();
				pid2.setFileIdx(hp2.getRelDef().getFileIdx());
				pid2.setPageIdx(head2.getListDataPageInfo().get(j).getPageIdx());
				
				ArrayList<Record> recs2 = hp2.getRecordsOnPage(pid2);
				
				for (Record r1 : recs1) {
					for (Record r2 : recs2) {
						
						if (r1.getValues().get(colId1 - 1).equals(r2.getValues().get(colId2 - 1))) {
							System.out.print(hp1.getRelDef().getNomRelation() + " : ");
							for (String str : r1.getValues()) {
								System.out.print(str + " ");
							}
							
							System.out.print(" | " + hp2.getRelDef().getNomRelation() + " : ");
							for (String str : r2.getValues()) {
								System.out.print(str + " ");
							}
							
							System.out.println("");
							n++;
						}
					}
				}
				
			}
		}
		
		System.out.println("Total records : " + n);

	}
	
	private void ExecCommandInsert(String [] args) {
		String nom_rel = args[1];
		FileManager fm = FileManager.getMonSingleton();	
		
		ArrayList<String> values = new ArrayList<>();
		
		for (int i = 2; i < args.length; i++) {
			values.add(args[i]);
		}
		
		Record rec = new Record();
		rec.setValues(values);
		
		Rid rid = fm.insertRecordInRelation(nom_rel, rec);
	}
	
	private void ExecCommandFill(String [] args)
	{
		FileManager fm = FileManager.getMonSingleton();
		String name = args[1];
		
		String path = Main.cheminDB + "/" + args[2];
		
		File file = new File(path);
				
		if(file.exists())
		{
			try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			    String line;
			    while ((line = br.readLine()) != null) {
			       Record rec = new Record();
			       rec.setValues(new ArrayList<String> (Arrays.asList(line.split(","))));
			       fm.insertRecordInRelation(name, rec);
			    }
			   
			}
			catch(Exception ex)
		    {
		    	System.out.println("ERROR FILL : " + ex.getMessage());
		    }
		}
		
	}
	
	private void ExecCommandCreate(String [] args) {
		String nom = args[1];
		int nb = Integer.parseInt(args[2]); 
		
		ArrayList<String> types = new ArrayList<>(); 
		
		for (int i = 0; i < nb; i++) {
			types.add(args[3+i]);
		}
		
		createRelation(nom, nb, types);
	}
	
	private void ExecCommandSelectAll(String [] args) {
		FileManager fm = FileManager.getMonSingleton();
		
		String nom = args[1];
		ArrayList<Record> liste = new ArrayList<>();
		
		liste = fm.getAllRecords(nom);
		for(Record rec : liste)
		{
			for(String val : rec.getValues())
			{
				System.out.print(val + " ");
			}
			System.out.println();
		}
		System.out.println("Total records : " + liste.size());
		
	}
	
	private void ExecCommandSelect(String [] args) {
		String nom = args[1];
		ArrayList<Record> liste = new ArrayList<>();
		
		liste = FileManager.getMonSingleton().getAllRecordsWithFilter(args[1], Integer.valueOf(args[2]), args[3]); //Change les args pour que ça corresponde aux types de la méthode
		for(Record rec : liste)
		{
			for(String val : rec.getValues())
			{
				System.out.print(val + " ");
			}
			System.out.println();
		}
		System.out.println("Total records : " + liste.size());
		
	}
	
	public void createRelation (String nomRelation, int nombreColonnes, ArrayList<String> typesDesColonnes)
	{
		
		RelDef rd = new RelDef();
		rd.setNomRelation(nomRelation);
		rd.setNbColonnes(nombreColonnes);
		rd.setTypesColonnes(typesDesColonnes);
		
		int recordSize = 0;
		
		for(int i = 0; i < typesDesColonnes.size(); i++)
		{
			if(typesDesColonnes.get(i).equals("int") || typesDesColonnes.get(i).equals("float"))
			{
				recordSize += 4;
			}
			else
			{	
				recordSize += 2 * Integer.valueOf(typesDesColonnes.get(i).substring(6));
			}
		}
		
		rd.setRecordSize(recordSize);
				
		rd.setSlotCount(Constants.pageSize / (rd.getRecordSize() + 1));
		
		
		DBDef dbd = DBDef.getMonSingleton();
		dbd.AddRelation(rd);
		rd.setFileIdx(dbd.getNRelDefs());
		
		FileManager.getMonSingleton().createNewHeapFile(rd);
	}
	
	public void clean() {
		File folder = null;

		folder = new File(Main.cheminDB);
				
		if (folder.listFiles() != null) {
			for (File f : folder.listFiles()) {
				if (f.getName().contains(".rf") || f.getName().contains("Catalog.def")) {
					try{		
		    
			    		if(f.delete()){
			    			System.out.println(f.getName() + " deleted!");
			    		}else{
			    			System.out.println(f.getName() + " NOT deleted!");
			    		}
			    	}catch(Exception e){
			    		e.printStackTrace();	
			    	}
				}
			}
		}
		
		BufferManager.getInstance().reset();
		DBDef.getMonSingleton().reset();
		FileManager.getMonSingleton().reset();
	}
}

import java.util.ArrayList;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class DBDef implements Serializable{
	
	private static final DBDef instance = new DBDef();
	
	private ArrayList<RelDef> defs;
	private int n_defs;
	
	private DBDef () {
		defs = new ArrayList<>();
		n_defs = 0;
	}
	
	public void reset() {
		defs = new ArrayList<>();
		n_defs = 0;
		this.Init();
	}
	
	 public static DBDef getMonSingleton() {
	        return instance;
	    }
	
	public void Init() {
		
		File file = new File(Main.cheminDB + "Catalog.def");
		if(file.exists())
		{
			try {
				FileInputStream fileIn = new FileInputStream(Main.cheminDB + "Catalog.def");
				ObjectInputStream objectIn = new ObjectInputStream(fileIn);
				DBDef obj = (DBDef)objectIn.readObject();
				this.defs = obj.getRelDefs();
				this.n_defs = obj.getNRelDefs();
				objectIn.close();
				fileIn.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		
	}
	
	public void Finish() {
			
		try {
			FileOutputStream fileOut = new FileOutputStream(Main.cheminDB + "Catalog.def");
			ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
			objectOut.writeObject(this);
			objectOut.close();
			objectOut.close();
			fileOut.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public void AddRelation(RelDef r) {
		defs.add(r);
		n_defs++;
	}
	
	public ArrayList<RelDef> getRelDefs() {
		return defs;
	}
	
	public int getNRelDefs() {
		return n_defs;
	}
	
	public void setRelDefs(ArrayList<RelDef> reldefs) {
		defs = reldefs;
	}
	
	public void setNRelDefs(int n_defs) {
		this.n_defs = n_defs;
	}
	
	
}

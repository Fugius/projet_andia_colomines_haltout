import java.nio.ByteBuffer;
import java.util.ArrayList;

public class HeaderPageInfo {
	private int dataPageCount;
	private ArrayList<DataPageInfo> listDataPageInfo = new ArrayList<DataPageInfo>(); 
 
	public HeaderPageInfo() {
		this.dataPageCount = 0;		
	}
	
	public void readFromBuffer(ByteBuffer b) { 	 
		b.position(0);
		
		dataPageCount = b.getInt();	
		
		listDataPageInfo = new ArrayList<DataPageInfo>();
		
		for (int i = 0; i < dataPageCount; i++) {
			int id = b.getInt();
			int freeSlots = b.getInt();
			
			DataPageInfo temp = new DataPageInfo();
			temp.setPageIdx(id);
			temp.setFreeSlots(freeSlots);
			listDataPageInfo.add(temp);
		}
		
		b.position(0);
		byte[] temps = new byte[(2 * dataPageCount + 1) * 4];
		b.get(temps);
		
	}
	
	public void writeToBuffer(ByteBuffer b) {
		b.position(0);
		b.putInt(dataPageCount); 
		
		for (int i = 0; i < listDataPageInfo.size() ; i++) {
			int id = listDataPageInfo.get(i).getPageIdx();
			int freeSlots = listDataPageInfo.get(i).getFreeSlots();
			
			b.putInt(id);
			b.putInt(freeSlots);
		}
	}
	
	public int getDataPageCount() {
		return dataPageCount;
	}

	public void setDataPageCount(int dataPageCount) {
		this.dataPageCount = dataPageCount;
	}

	public ArrayList<DataPageInfo> getListDataPageInfo() {
		return listDataPageInfo;
	}

	public void setListDataPageInfo(ArrayList<DataPageInfo> listDataPageInfo) {
		this.listDataPageInfo = listDataPageInfo;
	}
}

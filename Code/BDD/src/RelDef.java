import java.util.ArrayList;

public class RelDef implements java.io.Serializable {
	
	String nomRelation;
	int nbColonnes;
	ArrayList<String> typesColonnes;
	private int recordSize;
	private int slotCount;
	private int fileIdx;
	
	public RelDef()
	{
		nomRelation = new String();
		typesColonnes = new ArrayList<>();
	}

	public String getNomRelation() {
		return nomRelation;
	}

	public void setNomRelation(String relation) {
		this.nomRelation = relation;
	}

	public int getNbColonnes() {
		return nbColonnes;
	}

	public void setNbColonnes(int nbColonnes) {
		this.nbColonnes = nbColonnes;
	}

	public ArrayList<String> getTypesColonnes() {
		return typesColonnes;
	}

	public void setTypesColonnes(ArrayList<String> typesColonnes) {
		this.typesColonnes = typesColonnes;
	}

	public int getFileIdx() {
		return fileIdx;
	}

	public void setFileIdx(int fileIdx) {
		this.fileIdx = fileIdx;
	}

	public int getRecordSize() {
		return recordSize;
	}

	public void setRecordSize(int recordSize) {
		this.recordSize = recordSize;
	}

	public int getSlotCount() {
		return slotCount;
	}

	public void setSlotCount(int slotCount) {
		this.slotCount = slotCount;
	}
	
	
}

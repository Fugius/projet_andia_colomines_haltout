import java.nio.ByteBuffer;
import java.util.ArrayList;

public class BufferManager {
	
	private static final BufferManager instance = new BufferManager();
	
	private ArrayList<Frame> framePool = new ArrayList<>();
	private int freeFrames = 2;
	
	public static BufferManager getInstance() {
		return instance;
	}
	
	public void reset() {
		freeFrames = 2;
		framePool = new ArrayList<>();
	}
	
	private BufferManager () {
		
	}
	
	public ByteBuffer getPage(PageId pageID) {
		Frame fr = null;
		
		for (int i = 0; i < framePool.size(); i++) {
			PageId pID = framePool.get(i).getId();
			if (pID.getFileIdx() == pageID.getFileIdx() && pID.getPageIdx() == pageID.getPageIdx()) {
				fr = framePool.get(i);
			}
		}
		
		if (fr != null) {
			fr.incrementPinCount();
			return fr.getBuffer();
			
		} else if (freeFrames > 0) {
			DiskManager dm = DiskManager.getMonSingleton();
			fr = new Frame();
			fr.incrementPinCount();
			fr.setId(pageID);
			
			dm.ReadPage(pageID, fr.getBuffer());
			framePool.add(fr);
			freeFrames--;
			
			return framePool.get(framePool.size() - 1).getBuffer();
			
		} else {
			int LRU_Index = 0;
			long LRU_Time = framePool.get(0).getLastUse();
			
			for (int i = 0; i < framePool.size(); i++) {
				
				System.out.println("--------");
				System.out.println(framePool.get(i).getPin_count());
				
				if (framePool.get(i).getLastUse() < LRU_Time && framePool.get(i).isDirty()) {
					LRU_Index = i;
					LRU_Time = framePool.get(i).getLastUse();
				}
			}
			
			System.out.println("FINISHED LOOP");

			
			DiskManager dm = DiskManager.getMonSingleton();
			fr = new Frame();
			fr.incrementPinCount();
			fr.setId(pageID);
			
			//save old frame
			dm.WritePage(framePool.get(LRU_Index).getId(), framePool.get(LRU_Index).getBuffer());
			
			//set new frame
			dm.ReadPage(pageID, fr.getBuffer());
			framePool.set(LRU_Index, fr);
			
			return fr.getBuffer();
		}
		
		
	}
	
	public void freePage(PageId pageID, boolean isDirty) {
		for (int i = 0; i < framePool.size(); i++) {
			Frame fr = framePool.get(i);
			if (pageID.getFileIdx() == fr.getId().getFileIdx() && pageID.getPageIdx() == fr.getId().getPageIdx()) {
				fr.decreasePinCount();
				fr.setDirty(isDirty);
				freeFrames++;
			}
		}
	}
	
	public void flushAll() {
		for (int i = 0; i < framePool.size(); i++) {
			DiskManager dm = DiskManager.getMonSingleton();
			dm.WritePage(framePool.get(i).getId(), framePool.get(i).getBuffer());
		}
		
		freeFrames = Constants.pageSize;
		framePool.clear();
	}
	
	class Frame {
		private ByteBuffer buffer;
		private PageId id;
		private int pin_count;
		private boolean dirty_flag;
		private long lastUse;
		
		public Frame() {
			pin_count = 0;
			dirty_flag = false;
			id = new PageId();
			buffer = ByteBuffer.allocate(Constants.pageSize);
		}

		public ByteBuffer getBuffer() {
			lastUse = System.nanoTime();
			buffer.position(0);
			return buffer;
		}

		public void setBuffer(ByteBuffer buffer) {
			this.buffer = buffer;
			lastUse = System.nanoTime();
		}

		public PageId getId() {
			return id;
		}

		public void setId(PageId id) {
			this.id = id;
		}

		public int getPin_count() {
			return pin_count;
		}
		
		public void incrementPinCount() {
			pin_count++;
		}
		
		public void decreasePinCount() {
			pin_count--;
		}

		public boolean isDirty() {
			return dirty_flag;
		}

		public void setDirty(boolean dirty_flag) {
			lastUse = System.nanoTime();
			this.dirty_flag = dirty_flag;
		}

		public long getLastUse() {
			return lastUse;
		}

	}
}


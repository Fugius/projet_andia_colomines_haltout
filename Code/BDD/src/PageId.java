
public class PageId {
	
	private int PageIdx;
	private int FileIdx;
	
	public int getPageIdx() {
		return PageIdx;
	}
	
	public void setPageIdx(int pageIdx) {
		PageIdx = pageIdx;
	}
	
	public int getFileIdx() {
		return FileIdx;
	}
	
	public void setFileIdx(int fileIdx) {
		FileIdx = fileIdx;
	}
	
	
	
}
